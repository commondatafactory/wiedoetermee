// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React from 'react'
import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="nl">
        <Head>
          <meta charSet="utf-8" />
          <link rel="shortcut icon" href="/favicon.ico" />
          <meta name="theme-color" content="#000000" />
          <meta
            name="author"
            content="VNG, This Way Cartography, Niene Boeijen, Stephan Preeker, Ruben Werdmuller"
          ></meta>
          <meta
            name="description"
            content="Simpele applicatie om gemeenten in kaart te brengen en te exporteren naar pdf. Upload een Excel bestand met 1 kolom met daarin gemeente namen. Deze gemeente worden op de kaart ingekleurd. Pas de teksten aan en exporteer uw kaart naar PDF"
          />
          <script
            data-goatcounter="https://geitjes.commondatafactory.nl/count"
            async
            src="//geitjes.commondatafactory.nl/count.js"
          />
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
