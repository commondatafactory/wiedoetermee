// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { Dispatch, SetStateAction, FC, createContext, useState } from 'react'

export interface Item {
  add?: any
  input: string
  gm_code: string
  gm_name: string
  status?: 'o' | 'v' | 'x'
  onMap?: boolean
  complete?: boolean
}

export interface CountContextProps {
  mapData: Item[]
  setMapData: Dispatch<SetStateAction<CountContextProps['mapData']>>
  updateItem: Partial<Item> | undefined
  setUpdateItem: Dispatch<SetStateAction<CountContextProps['updateItem']>>
  fileLoaded: boolean
  setFileLoaded: Dispatch<SetStateAction<CountContextProps['fileLoaded']>>
  hoverFeature: string
  setHoverFeature: Dispatch<SetStateAction<CountContextProps['hoverFeature']>>
}

const AppContext = createContext<CountContextProps>({} as CountContextProps)

const AppContextProvider: FC = ({ children }) => {
  const [mapData, setMapData] = useState<Item[]>([])
  const [updateItem, setUpdateItem] = useState<Item>()
  const [fileLoaded, setFileLoaded] = useState(false)
  const [hoverFeature, setHoverFeature] = useState('')

  return (
    <AppContext.Provider
      value={{
        mapData,
        setMapData,
        updateItem,
        setUpdateItem,
        fileLoaded,
        setFileLoaded,
        hoverFeature,
        setHoverFeature,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export default AppContext
export { AppContextProvider }
