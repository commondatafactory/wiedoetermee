// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useRef, useEffect, useContext } from 'react'
import * as d3 from 'd3'

import AppContext from '../../../contextAPI'
import gemeenteJson2022 from '../../../data/gemeenten2022.json'
import gemeenteJson2023 from '../../../data/gemeenten2023.json'
import gemeenteJson2021 from '../../../data/gemeenten2021.json'

import * as Styled from './Map.styled'

const Map = function ({ year }) {
  const mapContainer = useRef(null)
  const divContainer = useRef(null)

  const itemWidth = 600
  const itemHeight = 700
  const fillColor = '#d2d2d2'
  const strokeColor = '#bebebe'
  const fillColor2 = '#0b71a1'
  const strokeColor2 = '#085a80'

  const { mapData, updateItem, fileLoaded, hoverFeature } = useContext(AppContext)

  // SVG settings
  useEffect(() => {
    d3.select(mapContainer.current)
      .attr('preserveAspectRatio', 'xMidYMid meet')
      .attr('viewBox', `0 0 ${itemWidth} ${itemHeight}`)
      .attr('title', 'First map ')
  }, [])

  // draw map
  useEffect(() => {
    if (mapContainer.current) {
      const svg = d3.select(mapContainer.current)
      const div = d3.select(divContainer.current)

      svg.selectAll('g').remove()

      let gemeenteJson
      // get year
      switch (year) {
        case '2023':
          gemeenteJson = gemeenteJson2023
          break
        case '2022':
          gemeenteJson = gemeenteJson2022
          break
        case '2021':
          gemeenteJson = gemeenteJson2021
          break
        default:
          break
      }

      // Projection & Path
      const projection = d3.geoMercator()
      projection.fitSize([itemWidth, itemHeight], gemeenteJson as any)
      const path = d3.geoPath().projection(projection)

      const tooltip = div
        .append('div')
        .style('opacity', 0)
        .attr('class', 'tooltip')
        .style('border', 'solid')
        .style('background-color', '#ffffff')
        .style('border-color', '#212121')
        .style('border-width', '1px')
        .style('border-radius', '5px')
        .style('padding', '5px')
        .style('position', 'absolute')

      const g = svg.append('g').classed('map-layer', true)
      g.selectAll('path')
        .data(gemeenteJson.features)
        .enter()
        .append('path')
        .attr('d', path as any)
        .attr('class', 'geodata')
        .attr('id', (d: { properties: { GM_CODE: any } }) => d.properties.GM_CODE)
        .style('fill', fillColor)
        .style('stroke', strokeColor)
        .style('stroke-width', '1px')
        .style('strok-linecap', 'round')
        .attr('pointer-events', 'none')
        .on('mouseover', function () {
          const color = d3.color(d3.select(this).style('fill'))
          const color2 = d3.color(d3.select(this).style('stroke'))
          d3.select(this)
            .style('fill', color?.darker(1.5) as any)
            .style('stroke', color2?.darker(1.5) as any)
            .raise()
          tooltip.style('opacity', 1)
        })
        .on('mousemove', function (e) {
          const name = e.originalTarget.__data__.properties.GM_NAAM
          tooltip
            .html(name)
            .style('left', e.pageX + 25 + 'px')
            .style('top', e.pageY + 10 + 'px')
        })
        .on('mouseout', function () {
          const color = d3.color(d3.select(this).style('fill'))
          const color2 = d3.color(d3.select(this).style('stroke'))

          d3.select(this)
            .style('fill', color?.brighter(1.5) as any)
            .style('stroke', color2?.brighter(1.5) as any)
          tooltip.style('opacity', 0).style('pointer-events', 'none')
        })
        .transition()
        .attr('pointer-events', 'auto')
    }

    // eslint-disable-next-line
  }, [year, itemHeight, itemWidth])

  useEffect(() => {
    const svg: any = d3.select(mapContainer.current)
    const ease = d3.transition().duration(750).ease(d3.easeCubic)

    if (mapData?.length > 1) {
      for (let i = 0; i < mapData.length; i++) {
        const element = mapData[Number(i)]
        if (element.gm_code && element.onMap) {
          svg
            .selectAll('g')
            .selectAll(`#${element.gm_code}`)
            .classed('active', true)
            .transition(ease)
            .style('fill', fillColor2)
            .style('stroke', strokeColor2)
        }
      }
    }
    // eslint-disable-next-line
  }, [mapData, year])

  useEffect(() => {
    if (updateItem) {
      const ease = d3.transition().duration(750).ease(d3.easeCubic)
      const svg: any = d3.select(mapContainer.current)
      // Add to map
      if (updateItem.add) {
        svg
          .selectAll('g')
          .selectAll('#' + updateItem.gm_code)
          .classed('active', true)
          .transition(ease)
          .style('fill', fillColor2)
          .style('stroke', strokeColor2)
      } else if (!updateItem.add) {
        svg
          .selectAll('g')
          .selectAll('#' + updateItem.gm_code)
          .classed('active', false)
          .transition(ease)
          .style('fill', fillColor)
          .style('stroke', strokeColor)
      }
    }
    // eslint-disable-next-line
  }, [updateItem])

  useEffect(() => {
    const svg: any = d3.select(mapContainer.current)
    const ease = d3.transition().duration(750).ease(d3.easeCubic)
    if (!fileLoaded) {
      svg
        .select('g')
        .selectAll('path')
        .classed('active', false)
        .transition(ease)
        .style('fill', fillColor)
        .style('stroke', strokeColor)
    }
    // eslint-disable-next-line
  }, [fileLoaded])

  useEffect(() => {
    const path = d3.select(mapContainer.current).selectAll('g')
    if (hoverFeature) {
      if (hoverFeature.length > 0) {
        // Reset first
        path.selectAll('path').style('fill', fillColor).style('stroke', strokeColor)

        path.selectAll('.active').style('fill', fillColor2).style('stroke', strokeColor2)

        // set color on hover
        path
          .selectAll('#' + hoverFeature)
          .style('fill', function () {
            const color = d3.color(d3.select(this).style('fill'))
            return color?.darker(1.5) as any
          })
          .style('stroke', function () {
            const color = d3.color(d3.select(this).style('fill'))
            return color?.darker(1.5) as any
          })
          .raise()
      }
    } else {
      path.selectAll('path').style('fill', fillColor).style('stroke', strokeColor)

      path.selectAll('.active').style('fill', fillColor2).style('stroke', strokeColor2)
    }
    // eslint-disable-next-line
  }, [hoverFeature])

  return (
    <Styled.Container id="map" ref={(el: any) => (divContainer.current = el)}>
      <svg version="1.1" ref={(el: any) => (mapContainer.current = el)} />
    </Styled.Container>
  )
}

export default Map
