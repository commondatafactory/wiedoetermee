// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const StyledTR = styled.tr<any>`
  pointer-events: ${(p) => (p.isDisabled ? 'none' : '')};

  &:hover {
    cursor: pointer;

    td {
      background-color: ${(div) => div.theme.tokens.colorPaletteGray200};
      font-weight: bold;
    }
  }
`

const StyledTd = styled.td<any>`
  font-weight: normal;

  :before {
    display: block;
    content: attr(title);
    font-weight: bold;
    height: 0;
    overflow: hidden;
    visibility: hidden;
  }
`

const CompleteItem = ({ item, handleRemove, handleAdd, setHoverFeature }: any) => {
  const [chkValue, setChkValue] = useState(false)

  useEffect(() => {
    setChkValue(item.onMap)
  }, [item])

  const handleClick = () => {
    !chkValue ? handleAdd(item) : handleRemove(item)
    setChkValue(!chkValue)
  }

  const handleMouseOver = () => {
    setHoverFeature(item.gm_code)
  }
  const handleMouseLeave = () => {
    setHoverFeature('')
  }

  return (
    <StyledTR
      onClick={handleClick}
      onMouseEnter={handleMouseOver}
      onMouseOut={handleMouseLeave}
      onMouseLeave={handleMouseLeave}
      isDisabled={item.status === 'x'}
      // style={item.status === 'x' ? { pointerEvents: 'none' } : { pointerEvents: 'all' }}
    >
      <StyledTd>
        <input readOnly type="checkbox" checked={chkValue} disabled={item.status === 'x'} />
      </StyledTd>
      <StyledTd title={item.input}>{item.input}</StyledTd>
      <StyledTd title={item.gm_name}>{item.gm_name}</StyledTd>
      <StyledTd title={item.gm_code}>{item.gm_code}</StyledTd>
    </StyledTR>
  )
}

export default CompleteItem
